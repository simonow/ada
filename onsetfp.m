function [onset_grid, onset_time, onset_loc] = onsetfp(onset_function, time_vector,MPH,MPP)
% This function outputs an Onset Grid of an Onset function with its time vector.
% The onsets of the onset_function are analysed with the MATLAB-Function findpeaks
% (Warning: use Matlab Version 2015 or higher because of new parameters).
% Another output is the onset_time and the discrete onset location
% (onset_loc). MPH stands for MinPeakHeight and MPP stands for
% MinPeakProminence. The onset Grid has the amplitude 2!

% normalize function for parameter functionality
onset_function=onset_function./(max(onset_function));
% invert function
onset_function=-1*onset_function;
% find the peaks (here onsets!!)
switch nargin
    case 4
    [~,onset_loc]=findpeaks(onset_function,'MinPeakHeight',MPH,'MinPeakProminence',MPP);
    case 3    
    [~,onset_loc]=findpeaks(onset_function,'MinPeakHeight',MPH);
    case 2
    [~,onset_loc]=findpeaks(onset_function);
end

onset_time=time_vector(onset_loc);
onset_grid=zeros(1,length(time_vector));

for n=1:length(time_vector)
    if find(onset_time==time_vector(n))>0
        onset_grid(n)=2;
    end
end

end

