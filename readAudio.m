function [x,fs] = readAudio(file)
% INPUTS:
%   file        ...     name of audio file (string) in mp3 or wav format
%
% 
% OUTPUTS:
%   x           ...     audio signale stored in variable x 
%   fs          ...     sampling frequency 
%
% USED FUNCTIONS:
%   
%
% DESCRIPTION:
%   The function reads an mp3 or wav format audio file. The audio signal is
%   stored in the variable x and the sampling frequency in the variable fs.
%   A Matlab version control is implemented to read the audio signal correctly. 

%% read WAV or MP3 file
filetype = file(end-2:end);
if (strcmp(filetype, 'mp3'))
    x = mp3read(file);   
else
    str = version;
    if str2num(str(1:3))<8.1 %before R2013a
        
        [x,fs] = wavread(file);
         
        % add audioread here if you are using a newer version of matlab :)
    else
        [x,fs] = audioread(file);
    end 
end
end

