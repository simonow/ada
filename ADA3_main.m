close all;
clear all;
clc;

% This project contains an Onset Detection Algorithm with several features.
% It tries to combine the features to the best result. 
% The explanations are in english, the plot captions are in german.

% Read Audio and define Parameters
[x,fs] = readAudio('fa_fa_va.wav');
x=(x(:,1)+x(:,2))/2;
n=0:length(x)-1;
t_x=n/fs;

% Weighted Phase Deviation and plot (uses function WPD.m)
hop=441;
N=2048;
[WPD t_WPD]=WPD(x,fs,N,hop);
clear hop N;

figure; 
plot(t_x,x,'color',[.6 .6 .6]);
title('Zeitsignal und WPD');
hold on;
plot(t_WPD,WPD,'LineWidth',2,'color',[0 0 0]);
xlabel('Zeit in Sekunden')
ylabel('Amplitude')

% Onset Detection and grid with findpeaks (other ideas?) and plot
WPDneg=-1*WPD;
[~,onset_loc]=findpeaks(WPDneg,'MinPeakHeight',-.4,'MinPeakProminence',0.08);
clear WPDneg;

onset_time=t_WPD(onset_loc);
onset_grid=zeros(1,length(t_WPD));

for nn=1:length(t_WPD)
    if find(onset_time==t_WPD(nn))>0
        onset_grid(nn)=2;
    end
end
clear nn;

figure; 
plot(t_x,x+1,'color',[.6 .6 .6]);
title('Zeitsignal und Onset Grid mit WPD')
hold on;
stem(t_WPD,onset_grid,'LineWidth',1.2,'color',[0 0 0],'Marker','none');
% plot(t_WPD,ones(1,length(t_WPD)),'LineWidth',1,'color',[0 0 0]);
plot(t_WPD,WPD+1,'LineWidth',2,'color',[0 0 0]);
xlabel('Zeit in Sekunden')
ylabel('Amplitude')
ax=gca;
ax.YTickLabel={'-1','-0.8','-0.6','-0.4','-0.2','0','0.2','0.4','0.6','0.8','1'};
clear ax;



