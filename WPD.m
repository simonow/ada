function [WPD,t_WPD] = WPD(x,fs,N,hop)
% The function takes an audio signal x (in mono or stereo), the sampling
% frequency fs, the fft-and window-length N and the hop size hop.
% The output is the Weighted Phase Deviation function WPD with 
% its time vector t_WPD
% Copyright Julian Linke

if size(x,2)==1
    x=x;
else
    x=(x(:,1)+x(:,2))/2;
end

len=length(x);
n=0:len-1;
t=n/fs;

w=hamming(N);

X=buffer(x,N,hop);
w=repmat(w,1,size(X,2));
Xw=X.*w;
Xfft=fft(Xw,N);
Xangle=angle(Xfft);
Xfft=Xfft(1:N/2,:);
Xangle=Xangle(1:N/2,:);

% instantaneous frequency Phi1
Phi1=zeros(N/2,size(X,2));
Phi1=Xangle-[zeros(N/2,1) Xangle(:,1:end-1)];

% instantaneous frequency Phi2
Phi2=zeros(N/2,size(X,2));
Phi2=Phi1-[zeros(N/2,1) Phi1(:,1:end-1)];

% WPD
WPD=(1/N)*sum(abs(Xfft.*Phi2));
% norm WPD
WPD=WPD./(max(WPD));
T=buffer(t,N,hop);
t_WPD=mean(T);
t_end=T(:,end);
t_end=mean(t_end(find(t_end>0)));
t_WPD(end)=t_end;

end

