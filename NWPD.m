function [t_NWPD,NWPDch1, NWPDch2] = NWPD(x,fs,N,hop)
% The function takes an audio signal x (in mono or stereo), the sampling
% frequency fs, the fft-and window-length N and the hop size hop.
% The output is the normalised Weighted Phase Deviation function NWPD with 
% its time vector t_NWPD. If the input signal has 2 channels the outputs
% are NWPDch1 and NWPDch2. If the input signal is mono, the NWPDch1 is the
% NWPD function.
% Copyright Julian Linke

if size(x,2)==1
    x=x;
    len=length(x);
    w=hamming(N);
    X=buffer(x,N,hop);
    w=repmat(w,1,size(X,2));
    Xw=X.*w;
    Xfft=fft(Xw,N);
    Xangle=angle(Xfft);
    Xfft=Xfft(1:N/2,:);
    Xangle=Xangle(1:N/2,:);
    Phi1=zeros(N/2,size(X,2));
    Phi1=Xangle-[zeros(N/2,1) Xangle(:,1:end-1)];
    Phi2=zeros(N/2,size(X,2));
    Phi2=Phi1-[zeros(N/2,1) Phi1(:,1:end-1)];
    NWPDch1=(sum(abs(Xfft.*Phi2)))./(sum(abs(Xfft)));
    t_NWPD=zeros(1,length(NWPDch1));
    
    % Time Vector
    for i=1:length(NWPDch1)
       t_NWPD(i)=i*(len/length(NWPDch1))/fs;
    end
else
    xch1=x(:,1); % channel 1
    xch2=x(:,2); % channel 2
    len=length(xch1);

    % Channel 1
    w=hamming(N);
    X=buffer(xch1,N,hop);
    w=repmat(w,1,size(X,2));
    Xw=X.*w;
    Xfft=fft(Xw,N);
    Xangle=angle(Xfft);
    Xfft=Xfft(1:N/2,:);
    Xangle=Xangle(1:N/2,:);
    Phi1=zeros(N/2,size(X,2));
    Phi1=Xangle-[zeros(N/2,1) Xangle(:,1:end-1)];
    Phi2=zeros(N/2,size(X,2));
    Phi2=Phi1-[zeros(N/2,1) Phi1(:,1:end-1)];
    NWPDch1=(sum(abs(Xfft.*Phi2)))./(sum(abs(Xfft)));
    clear X Xw Xfft Xangle Phi1 Phi2 w

    % Channel 2
    w=hamming(N);
    X=buffer(xch2,N,hop);
    w=repmat(w,1,size(X,2));
    Xw=X.*w;
    Xfft=fft(Xw,N);
    Xangle=angle(Xfft);
    Xfft=Xfft(1:N/2,:);
    Xangle=Xangle(1:N/2,:);
    Phi1=zeros(N/2,size(X,2));
    Phi1=Xangle-[zeros(N/2,1) Xangle(:,1:end-1)];
    Phi2=zeros(N/2,size(X,2));
    Phi2=Phi1-[zeros(N/2,1) Phi1(:,1:end-1)];
    NWPDch2=(sum(abs(Xfft.*Phi2)))./(sum(abs(Xfft)));

    % Time Vector
    t_NWPD=zeros(1,length(NWPDch1));
    for i=1:length(NWPDch1)
        t_NWPD(i)=i*(len/length(NWPDch1))/fs;
    end
end
end

